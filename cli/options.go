package cli

import (
	"github.com/jessevdk/go-flags"
)

var Options General

var Parser = flags.NewParser(&Options, flags.Default)

type Pipegraph struct {
	Start           PipegraphStart           `command:"start" description:"Start a pipegraph"`
	List            PipegraphList            `command:"list" alias:"ls" description:"List all pipegraphs"`
	Stop            PipegraphStop            `command:"stop" description:"Stop a pipegraph"`
	Instances       PipegraphInstances       `command:"instances" alias:"lsi" description:"List all instances of pipegraph"`
	Inspect         PipegraphInspect         `command:"inspect" alias:"i" description:"Inspect a pipegraph"`
	InspectInstance PipegraphInspectInstance `command:"inspect-instance" alias:"ii" description:"Inspect an instance of a pipegraph"`
}

type PipegraphStart struct {
	Positional struct {
		PipegraphName string
	} `positional-args:"true" required:"1"`
}

type PipegraphInspect struct {
	Positional struct {
		PipegraphName string
	} `positional-args:"true" required:"1"`
}

type PipegraphInspectInstance struct {
	Positional struct {
		PipegraphName string
		InstanceName string
	} `positional-args:"true" required:"2"`
}

type PipegraphStop struct {
	Positional struct {
		PipegraphName string
	} `positional-args:"true" required:"1"`
}

type PipegraphList struct {
	Quiet bool `short:"q" long:"quiet" description:"print only name of pipegraph"`
}

type PipegraphInstances struct {
	Positional struct {
		PipegraphName string `positional-arg-name:"instance-name"`
	}`positional-args:"true" required:"true"`
}

type General struct {
	MasterUrl string    `short:"m" long:"master-url" default:"http://localhost:2891" env:"WASP_MASTER_URL" description:"Url of wasp master instance rest endpoint"`
	Pipegraph Pipegraph `command:"pipegraph" description:"Manage pipegraphs"`
}
