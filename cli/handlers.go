package cli

import (
	"fmt"
	"gopkg.in/resty.v1"
	"github.com/apcera/termtables"
	"time"
	"errors"
	"encoding/json"
)

type PipegraphModel struct {
	Name                          string             `json:"name"`
	Description                   string             `json:"description"`
	Owner                         string             `json:"owner"`
	IsSystem                      bool               `json:"isSystem"`
	CreationTime                  int64              `json:"creationTime"`
	LegacyStreamingComponents     []*json.RawMessage `json:"legacyStreamingComponents"`
	StructuredStreamingComponents []*json.RawMessage `json:"structuredStreamingComponents"`
	RtComponents                  []*json.RawMessage `json:"rtComponents"`
}

type PipegraphInstanceModel struct {
	Name                   string `json:"name"`
	InstanceOf             string `json:"instanceOf"`
	StartTimestamp         int64  `json:"startTimestamp"`
	CurrentStatusTimestamp int64  `json:"currentStatusTimestamp"`
	Status                 string `json:"status"`
	Error                  string `json:"error,omitempty"`
}

type ListResult struct {
	Result string           `json:"Result"`
	Data   []PipegraphModel `json:"data"`
}

type InstancesResult struct {
	Result string                   `json:"Result"`
	Data   []PipegraphInstanceModel `json:"data"`
}

type StopResult struct {
	Result string `json:"Result"`
	Data   string `json:"data"`
}

type StopError struct {
	Result       string `json:"Result"`
	ErrorMessage string `json:"ErrorMsg"`
}

type StartResult struct {
	Result string `json:"Result"`
	Data struct {
		Instance string `json:"instance"`
		Result   string `json:"startResult"`
	} `json:"data"`
}

type InstanceInspectResult struct {
	Result string                 `json:"Result"`
	Data   PipegraphInstanceModel `json:"data"`
}

type InstanceInspectError struct {
	Result       string `json:"Result"`
	ErrorMessage string `json:"ErrorMsg"`
}

type StartError struct {
	Result       string `json:"Result"`
	ErrorMessage string `json:"ErrorMsg"`
}

func fromMillis(millis int64) string {
	seconds := millis / 1000
	nanos := (millis % 1000) * 1000

	return time.Unix(seconds, nanos).Format(time.UnixDate)
}

func (*PipegraphStop) Execute(args []string) error {
	var result StopResult
	var errorResult StopError

	url := fmt.Sprintf("%s/%s/%s/%s", Options.MasterUrl, "pipegraphs", Options.Pipegraph.Stop.Positional.PipegraphName, "stop")
	req, err := resty.R().SetResult(&result).SetError(&errorResult).Post(url)
	if err != nil {
		return err
	}

	if req.StatusCode() == 200 {
		println(result.Data)
		return nil
	}

	return errors.New(errorResult.ErrorMessage)
}

func (*PipegraphStart) Execute(args []string) error {
	var result StartResult
	var errorResult StartError

	url := fmt.Sprintf("%s/%s/%s/%s", Options.MasterUrl, "pipegraphs", Options.Pipegraph.Start.Positional.PipegraphName, "start")
	req, err := resty.R().SetResult(&result).SetError(&errorResult).Post(url)
	if err != nil {
		return err
	}

	if req.StatusCode() == 200 {
		println(result.Data.Instance)
		return nil
	}

	return errors.New(errorResult.ErrorMessage)
}

func (*PipegraphInstances) Execute(args []string) error {
	var result InstancesResult
	url := fmt.Sprintf("%s/%s/%s/%s", Options.MasterUrl, "pipegraphs", Options.Pipegraph.Instances.Positional.PipegraphName, "instances")
	_, err := resty.R().SetResult(&result).Get(url)

	if err != nil {
		return err
	}

	termtables.EnableUTF8PerLocale()
	table := termtables.CreateTable()

	table.AddHeaders("Name", "Instance of", "Started", "Last Status Update", "Status", "Error")

	for _, element := range result.Data {
		table.AddRow(
			element.Name,
			element.InstanceOf,
			fromMillis(element.StartTimestamp),
			fromMillis(element.CurrentStatusTimestamp),
			element.Status,
			element.Error,
		)
	}

	print(table.Render())

	return nil
}

func (*PipegraphList) Execute(args []string) error {
	var result ListResult
	url := fmt.Sprintf("%s/%s", Options.MasterUrl, "pipegraphs")
	_, err := resty.R().SetResult(&result).Get(url)

	if err != nil {
		return err
	}

	if Options.Pipegraph.List.Quiet {
		for _, element := range result.Data {
			println(element.Name)
		}
	} else {

		termtables.EnableUTF8PerLocale()
		table := termtables.CreateTable()

		table.AddHeaders("Name", "Description", "Owner", "IsSystem", "CreationTime")

		for _, element := range result.Data {
			table.AddRow(
				element.Name,
				element.Description,
				element.Owner,
				element.IsSystem,
				fromMillis(element.CreationTime),
			)
		}

		print(table.Render())
	}
	return nil
}

func (*PipegraphInspect) Execute(args []string) error {
	var result ListResult
	url := fmt.Sprintf("%s/%s", Options.MasterUrl, "pipegraphs")
	_, err := resty.R().SetResult(&result).Get(url)

	if err != nil {
		return err
	}

	found := false

	for _, element := range result.Data {
		if element.Name == Options.Pipegraph.Inspect.Positional.PipegraphName {
			b, err := json.MarshalIndent(element, "", "  ")
			if err != nil {
				return err
			}
			fmt.Println(string(b))
			found = true
		}
	}

	if !found {
		return errors.New("Pipegraph not found")
	}

	return nil
}

func (*PipegraphInspectInstance) Execute(args []string) error {
	var result InstanceInspectResult
	var resultError InstanceInspectError

	url := fmt.Sprintf(
		"%s/%s/%s/%s/%s",
		Options.MasterUrl,
		"pipegraphs",
		Options.Pipegraph.InspectInstance.Positional.PipegraphName,
		"instances", Options.Pipegraph.InspectInstance.Positional.InstanceName,
	)
	req, err := resty.R().SetResult(&result).SetError(&resultError).Get(url)

	if err != nil {
		return err
	}

	if req.StatusCode() != 200 {
		return errors.New(resultError.ErrorMessage)
	}

	termtables.EnableUTF8PerLocale()
	table := termtables.CreateTable()

	table.AddHeaders("Key", "Value")

	table.AddRow("Name", result.Data.Name)
	table.AddRow("Instance Of", result.Data.InstanceOf)
	table.AddRow("Status", result.Data.Status)
	table.AddRow("Started", fromMillis(result.Data.StartTimestamp))
	table.AddRow("Last Status Update", fromMillis(result.Data.CurrentStatusTimestamp))
	table.AddRow("Error", result.Data.Error)

	print(table.Render())

	return nil
}
